defmodule Tasks do
  @moduledoc """
  Tasks keeps the contexts that define your domain
  and business logic.

  Contexts are also responsible for managing your data, regardless
  if it comes from the database, an external API or others.
  """

  alias Tasks.Api
  alias Tasks.Api.Task

  use Timex

  @doc """
  Validates the length of the value of the indicated field (key) of the map
  """
  def validateString({:ok, map}, key) do
    cond do
      String.length(map[key]) > 2 -> {:ok, map}
      String.length(map[key]) <= 2 -> {:error, "#{key} (#{map[key]}) must have more of 2 chars"}
    end
  end

  def validateString({:error, message}, _) do
    {:error, message}
  end

  @doc """
  Cast the string value to the correct type
  """
  def convert({:ok, map}) do
    map = Map.put(map, "value", convert(map["value"]))
    {:ok, map}
  end

  def convert("true"), do: true
  def convert("false"), do: false

  def convert(num) when is_bitstring(num) do
    case Integer.parse(num) do
      {_, ""} -> String.to_integer(num)
      _ -> num
    end
  end

  def convert({:error, message}) do
    {:error, message}
  end

  @doc """
  Updates the map putting the field "type" and his value
  """
  def setType({:ok, map}) do
    map = Map.put(map, "type", getType(map["value"]))
    {:ok, map}
  end

  def setType({:error, message}) do
    {:error, message}
  end

  defp getType(value) when is_boolean(value) do
    "boolean"
  end

  defp getType(value) when is_integer(value) do
    "integer"
  end

  defp getType(value) when is_bitstring(value) do
    "string"
  end

  @doc """
  Validates the length of field "value" if the type is "string"
  """
  def validateStringValue({:ok, map}) do
    cond do
      map["type"] != "string" -> {:ok, map}
      map["type"] == "string" -> validateString({:ok, map}, "value")
    end
  end

  def validateStringValue({:error, message}) do
    {:error, message}
  end

  @doc """
  Validates if the subject / action of the map is present in a previous record
  """
  def validateExist({:ok, map}) do
    Api.get_first_action_by_subject(map["subject"], map["action"])
    |> case do
      nil -> {:ok, map}
      x -> compareType({:ok, map}, x.type)
    end
  end

  def validateExist({:error, message}) do
    {:error, message}
  end

  @doc """
  Validates if the action of the map is equals to a previous record
  """
  def compareType({:ok, map}, type) do
    cond do
      map["type"] == type -> {:ok, map}
      map["type"] != type -> {:error, "Invalid type for #{map["action"]}, must be a #{type}"}
    end
  end

  @doc """
  Inserts in database a new record with de map data
  """
  def insert({:ok, map}) do
    action_params = %{
      subject: map["subject"],
      action: map["action"],
      str_value: to_string(map["value"]),
      type: map["type"]
    }

    with {:ok, %Task{} = action} <- Api.create_task(action_params) do
      {:ok, action}
    end
  end

  def insert({:error, message}) do
    {:error, message}
  end

  @doc """
  Routes the process according to the type of the request
  """
  def processType({:ok, list}) do
    cond do
      hd(list).type == "integer" -> processInteger({:ok, list})
      hd(list).type == "boolean" -> processBoolean({:ok, list})
      hd(list).type == "string" -> processString({:ok, list})
    end
  end

  def processType({:error, message}) do
    {:error, message}
  end

  @doc """
  Process the integer request
  """
  def processInteger({:ok, list}) do
    value =
      groupBySeconds(list)
      |> Enum.map(fn item -> %{time: hd(item).inserted_at, value: processIntegerList(item)} end)

    {:ok, value}
  end

  @doc """
  Builds the integer response
  """
  def processIntegerList(list) do
    values = Enum.map(list, fn record -> String.to_integer(record.str_value) end)
    %{max: Enum.max(values), min: Enum.min(values), average: getAverage(values)}
  end

  @doc """
  Returns the average of the integer list
  """
  def getAverage(values) do
    (Enum.sum(values) / length(values))
    |> Float.ceil(2)
  end

  @doc """
  Process the boolean request and build the response
  """
  def processBoolean({:ok, list}) do
    values =
      Enum.map(list, fn record ->
        %{time: record.inserted_at, value: convert(record.str_value)}
      end)
      |> Enum.sort()
      |> Enum.dedup_by(fn record -> record.value end)

    {:ok, values}
  end

  @doc """
  Process the string request
  """
  def processString({:ok, list}) do
    value =
      groupBySeconds(list)
      |> Enum.map(fn item -> %{time: hd(item).inserted_at, value: processStringList(item)} end)

    {:ok, value}
  end

  @doc """
  Groups records per second of insertion
  """
  def groupBySeconds(list) do
    Enum.sort_by(list, fn row -> row.inserted_at end, :asc)
    |> Enum.chunk_by(fn record ->
      # DateTime.from_naive!(record.inserted_at, "Etc/UTC")
      Timex.to_datetime(record.inserted_at)
      |> DateTime.truncate(:second)
    end)
  end

  @doc """
  Build the string response
  """
  def processStringList(list) do
    Enum.reduce(list, "", fn x, acc -> x.str_value <> acc end)
    |> String.graphemes()
    |> Enum.frequencies()
    |> Enum.map(fn {k, x} -> %{key: k, value: x} end)
    |> Enum.sort_by(fn row -> row.value end, :desc)
    |> Enum.reduce("", fn x, acc -> acc <> x.key end)
  end
end
