defmodule Tasks.Api.Task do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tasks" do
    field :action, :string
    field :str_value, :string
    field :subject, :string
    field :type, :string

    timestamps()
  end

  @doc false
  def changeset(task, attrs) do
    task
    |> cast(attrs, [:subject, :action, :str_value, :type])
    |> validate_required([:subject, :action, :str_value, :type])
  end
end
