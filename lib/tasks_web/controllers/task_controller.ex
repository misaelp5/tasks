defmodule TasksWeb.TaskController do
  use TasksWeb, :controller

  alias Tasks.Api

  import Tasks

  action_fallback TasksWeb.FallbackController

  @doc """
  Render a json with the data of the element of the requested id.
  """
  def show(conn, %{"id" => id}) do
    task = Api.get_task!(id)
    render(conn, "show.json", task: task)
  end

  @doc """
  Insert a record on database with the request data
  """
  def insert(conn, %{"action" => action, "subject" => subject, "value" => value}) do
    map = %{"action" => action, "subject" => subject, "value" => value}

    validateString({:ok, map}, "subject")
    |> validateString("action")
    |> convert
    |> setType
    |> validateStringValue
    |> validateExist
    |> insert
    |> respond(conn)
  end

  @doc """
  Returns a json representation of the inserted record
  """
  def respond({:ok, action}, conn) do
    conn
    |> put_status(:created)
    |> put_resp_header("location", Routes.task_path(conn, :show, action))
    |> render("show.json", task: action)
  end

  def respond({:error, message}, conn) do
    json(conn, %{Error: message})
  end

  @doc """
  Returns a json with the data of the subject / action requested
  """
  def query(conn, %{"action" => action, "subject" => subject}) do
    Api.get_action_by_subject(subject, action)
    |> case do
      [] -> {:error, "Dont exist data for #{subject} with #{action}"}
      list -> {:ok, list}
    end
    |> processType
    |> respondJson(conn)
  end

  @doc """
  Returns a respond with a json representation of the message
  """
  def respondJson({:ok, message}, conn) do
    json(conn, %{data: message})
  end

  def respondJson({:error, message}, conn) do
    json(conn, %{error: message})
  end
end
