defmodule TasksWeb.TaskView do
  use TasksWeb, :view
  alias TasksWeb.TaskView

  def render("index.json", %{tasks: tasks}) do
    %{data: render_many(tasks, TaskView, "task.json")}
  end

  def render("show.json", %{task: task}) do
    %{data: render_one(task, TaskView, "task.json")}
  end

  def render("task.json", %{task: task}) do
    %{
      id: task.id,
      subject: task.subject,
      action: task.action,
      str_value: task.str_value,
      type: task.type
    }
  end
end
