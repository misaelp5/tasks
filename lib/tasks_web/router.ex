defmodule TasksWeb.Router do
  use TasksWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", TasksWeb do
    pipe_through :api

    get "/tasks/:id", TaskController, :show
    get "/:subject/:action/:value", TaskController, :insert
    get "/:subject/:action", TaskController, :query
  end
end
