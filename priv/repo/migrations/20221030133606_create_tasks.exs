defmodule Tasks.Repo.Migrations.CreateTasks do
  use Ecto.Migration

  def change do
    create table(:tasks) do
      add :subject, :string
      add :action, :string
      add :str_value, :string
      add :type, :string

      timestamps()
    end
  end
end
