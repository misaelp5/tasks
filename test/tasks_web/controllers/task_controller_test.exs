defmodule TasksWeb.TaskControllerTest do
  use TasksWeb.ConnCase
  # use Tasks.DataCase

  import Tasks.TasksFixtures
  import TasksWeb.TaskController

  describe "Test query method" do
    test "Return json with requested data", %{conn: conn} do
      insertIntegerExampleTask()
      conn = get(conn, Routes.task_path(conn, :query, "the_subject", "the_action"))

      assert [
               %{
                 "time" => _,
                 "value" => %{
                   "average" => 20.0,
                   "max" => 30,
                   "min" => 10
                 }
               }
             ] = json_response(conn, 200)["data"]
    end

    test "Return no-data message", %{conn: conn} do
      conn = get(conn, Routes.task_path(conn, :query, "the_subject", "the_action"))
      assert %{"error" => _} = json_response(conn, 200)
    end
  end

  describe "Test show controller" do
    test "Return json with requested id", %{conn: conn} do
      task = task_fixture()
      conn = get(conn, Routes.task_path(conn, :show, task.id))
      assert task.id == json_response(conn, 200)["data"]["id"]
    end
  end

  describe "Test insert controller" do
    test "Return json with inserted data on integer test", %{conn: conn} do
      conn = get(conn, Routes.task_path(conn, :insert, "subject", "action", "100"))
      assert "integer" == json_response(conn, 201)["data"]["type"]
    end

    test "Return json with inserted data on boolean test", %{conn: conn} do
      conn = get(conn, Routes.task_path(conn, :insert, "subject", "action", "true"))
      assert "boolean" == json_response(conn, 201)["data"]["type"]
    end

    test "Return json with inserted data on string test", %{conn: conn} do
      conn = get(conn, Routes.task_path(conn, :insert, "subject", "action", "abcd"))
      assert "string" == json_response(conn, 201)["data"]["type"]
    end

    test "Return error with bad params", %{conn: conn} do
      conn = get(conn, Routes.task_path(conn, :insert, "subject", "action", "ab"))
      assert %{"Error" => _} = json_response(conn, 200)
    end
  end
end
