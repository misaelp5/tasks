defmodule Tasks.TasksFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Tasks.Api` context.
  """

  @doc """
  Generate a task.
  """
  def task_fixture(attrs \\ %{}) do
    {:ok, task} =
      attrs
      |> Enum.into(%{
        action: "the_action",
        str_value: "10",
        subject: "the_subject",
        type: "integer"
      })
      |> Tasks.Api.create_task()

    task
  end

  def insertIntegerExampleTask(attrs \\ %{}) do

      attrs
      |> Enum.into(%{
        action: "the_action",
        str_value: "10",
        subject: "the_subject",
        type: "integer"
      })
      |> Tasks.Api.create_task()


      attrs
      |> Enum.into(%{
        action: "the_action",
        str_value: "20",
        subject: "the_subject",
        type: "integer"
      })
      |> Tasks.Api.create_task()


      attrs
      |> Enum.into(%{
        action: "the_action",
        str_value: "30",
        subject: "the_subject",
        type: "integer"
      })
      |> Tasks.Api.create_task()


  end
end
