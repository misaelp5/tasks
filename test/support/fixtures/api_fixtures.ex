defmodule Tasks.ApiFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Tasks.Api` context.
  """

  @doc """
  Generate a task.
  """
  def task_fixture(attrs \\ %{}) do
    {:ok, task} =
      attrs
      |> Enum.into(%{
        action: "some action",
        str_value: "some str_value",
        subject: "some subject",
        type: "some type"
      })
      |> Tasks.Api.create_task()

    task
  end
end
