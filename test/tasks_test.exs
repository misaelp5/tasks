defmodule TasksTest do
  use Tasks.DataCase
  use Timex

  import Tasks
  import Tasks.TasksFixtures

  describe "Get average of list" do
    test "return the averages of the elements" do
      assert getAverage([10, 20, 30]) == 20
    end
  end

  describe "Test Validate String" do
    test "return when text has more of two chars" do
      assert validateString({:ok, %{"testText" => "abc"}}, "testText") ==
               {:ok, %{"testText" => "abc"}}
    end

    test "Return error when text has less of two chars" do
      assert {:error, _} = validateString({:ok, %{"testText" => "ab"}}, "testText")
    end

    test "Return error when recieve error" do
      assert {:error, _} = validateString({:error, "xyz"}, "")
    end
  end

  describe "Test convert method" do
    test "Convert a boolean true" do
      assert convert({:ok, %{"value" => "true"}}) == {:ok, %{"value" => true}}
    end

    test "Convert a boolean false" do
      assert convert({:ok, %{"value" => "false"}}) == {:ok, %{"value" => false}}
    end

    test "Convert a integer" do
      assert convert({:ok, %{"value" => "10"}}) == {:ok, %{"value" => 10}}
    end

    test "Convert a string" do
      assert convert({:ok, %{"value" => "abcd"}}) == {:ok, %{"value" => "abcd"}}
    end

    test "Return error when recieve error" do
      assert {:error, _} = convert({:error, "xyz"})
    end
  end

  describe "Test Set type method" do
    test "Get the correct type with integer" do
      assert setType({:ok, %{"value" => 100}}) == {:ok, %{"value" => 100, "type" => "integer"}}
    end

    test "Get the correct type with boolean" do
      assert setType({:ok, %{"value" => true}}) == {:ok, %{"value" => true, "type" => "boolean"}}
    end

    test "Get the correct type with string" do
      assert setType({:ok, %{"value" => "abcd"}}) ==
               {:ok, %{"value" => "abcd", "type" => "string"}}
    end

    test "Return error when recieve error" do
      assert {:error, _} = setType({:error, "xyz"})
    end
  end

  describe "Test the value length validation method when type is string" do
    test "Return map when the value is correct" do
      assert validateStringValue({:ok, %{"type" => "string", "value" => "abcd"}}) ==
               {:ok, %{"type" => "string", "value" => "abcd"}}
    end

    test "Return map when the value is not string" do
      assert validateStringValue({:ok, %{"type" => "boolean", "value" => "true"}}) ==
               {:ok, %{"type" => "boolean", "value" => "true"}}
    end

    test "Return error when the value is incorrect" do
      assert {:error, _} = validateStringValue({:ok, %{"type" => "string", "value" => "ab"}})
    end

    test "Return error when recieve error" do
      assert {:error, _} = validateStringValue({:error, "xyz"})
    end
  end

  describe "Test method compareType" do
    test "Return map when the type is equals" do
      assert compareType({:ok, %{"type" => "string"}}, "string") == {:ok, %{"type" => "string"}}
    end

    test "Return error when the type is not equals" do
      assert {:error, _} = compareType({:ok, %{"type" => "string", "action" => "xyz"}}, "boolean")
    end
  end

  describe "Test process type method" do
    test "Returns a list with the time of the value changes when receiving a list of boolean values" do
      booleanMap = [
        %{
          subject: "test",
          action: "open",
          str_value: "true",
          type: "boolean",
          inserted_at: "~N[2016-05-24 13:26:08.003]"
        },
        %{
          subject: "test",
          action: "open",
          str_value: "false",
          type: "boolean",
          inserted_at: "~N[2016-05-24 13:26:10.003]"
        },
        %{
          subject: "test",
          action: "open",
          str_value: "false",
          type: "boolean",
          inserted_at: "~N[2016-05-24 13:26:12.003]"
        },
        %{
          subject: "test",
          action: "open",
          str_value: "true",
          type: "boolean",
          inserted_at: "~N[2016-05-24 13:26:20.003]"
        }
      ]

      assert processType({:ok, booleanMap}) ==
               {:ok,
                [
                  %{time: "~N[2016-05-24 13:26:08.003]", value: true},
                  %{time: "~N[2016-05-24 13:26:10.003]", value: false},
                  %{time: "~N[2016-05-24 13:26:20.003]", value: true}
                ]}
    end

    test "Return a list with max, min and average of the elements" do
      datetime = Timex.now()

      integerMap = [
        %{
          subject: "test",
          action: "open",
          str_value: "10",
          type: "integer",
          inserted_at: datetime
        },
        %{
          subject: "test",
          action: "open",
          str_value: "20",
          type: "integer",
          inserted_at: datetime
        },
        %{
          subject: "test",
          action: "open",
          str_value: "30",
          type: "integer",
          inserted_at: datetime
        }
      ]

      assert processType({:ok, integerMap}) ==
               {:ok,
                [
                  %{time: datetime, value: %{average: 20.0, max: 30, min: 10}}
                ]}
    end

    test "Return a string order by char occurrences" do
      datetime = Timex.now()

      stringMap = [
        %{
          subject: "test",
          action: "open",
          str_value: "aaabbbccc",
          type: "string",
          inserted_at: datetime
        },
        %{
          subject: "test",
          action: "open",
          str_value: "aaabbb",
          type: "string",
          inserted_at: datetime
        },
        %{
          subject: "test",
          action: "open",
          str_value: "aaa",
          type: "string",
          inserted_at: datetime
        }
      ]

      assert processType({:ok, stringMap}) ==
               {:ok,
                [
                  %{time: datetime, value: "abc"}
                ]}
    end

    test "Return error when recieve error" do
      assert {:error, _} = processType({:error, "xyz"})
    end
  end

  describe "Test processIntegerList method" do
    test "Response a mix, max and avg array of the list" do
      integerMap = [
        %{
          subject: "test",
          action: "open",
          str_value: "10",
          type: "integer",
          inserted_at: "~N[2016-05-24 13:26:08.003]"
        },
        %{
          subject: "test",
          action: "open",
          str_value: "20",
          type: "integer",
          inserted_at: "~N[2016-05-24 13:26:10.003]"
        },
        %{
          subject: "test",
          action: "open",
          str_value: "30",
          type: "integer",
          inserted_at: "~N[2016-05-24 13:26:12.003]"
        }
      ]

      assert processIntegerList(integerMap) == %{max: 30, min: 10, average: 20}
    end
  end

  describe "Test processStringList method" do
    test "Response a string order by char occurrences" do
      stringMap = [
        %{
          subject: "test",
          action: "open",
          str_value: "aaabbbccc",
          type: "string",
          inserted_at: "~N[2016-05-24 13:26:08.003]"
        },
        %{
          subject: "test",
          action: "open",
          str_value: "aaabbb",
          type: "string",
          inserted_at: "~N[2016-05-24 13:26:10.003]"
        },
        %{
          subject: "test",
          action: "open",
          str_value: "aaa",
          type: "string",
          inserted_at: "~N[2016-05-24 13:26:12.003]"
        }
      ]

      assert processStringList(stringMap) == "abc"
    end
  end

  describe "Test validate if record exists" do
    test "Return record if exists" do
      task = task_fixture()

      {:ok, dbtask} =
        validateExist(
          {:ok,
           %{
             "subject" => "the_subject",
             "action" => "the_action",
             "value" => "10",
             "type" => "integer"
           }}
        )

      assert dbtask["action"] == task.action
      assert dbtask["value"] == task.str_value
      assert dbtask["subject"] == task.subject
      assert dbtask["type"] == task.type
    end

    test "Mantain record if not exists" do
      {:ok, dbtask} =
        validateExist(
          {:ok,
           %{
             "subject" => "the_subject",
             "action" => "the_action",
             "value" => "10",
             "type" => "integer"
           }}
        )

      assert dbtask["action"] == "the_action"
      assert dbtask["value"] == "10"
      assert dbtask["subject"] == "the_subject"
      assert dbtask["type"] == "integer"
    end

    test "Return error when recieve error" do
      assert {:error, _} = validateExist({:error, "xyz"})
    end
  end

  describe "Test insert method" do
    test "Return the record inserted" do
      task = task_fixture()

      {:ok, dbtask} =
        insert(
          {:ok,
           %{
             "subject" => "the_subject",
             "action" => "the_action",
             "value" => "10",
             "type" => "integer"
           }}
        )

      assert dbtask.action == task.action
      assert dbtask.str_value == task.str_value
      assert dbtask.subject == task.subject
      assert dbtask.type == task.type
    end

    test "Return error when recieve error" do
      assert {:error, _} = insert({:error, "xyz"})
    end
  end
end
