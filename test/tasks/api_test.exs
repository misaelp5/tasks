defmodule Tasks.ApiTest do
  use Tasks.DataCase

  alias Tasks.Api

  describe "tasks" do
    alias Tasks.Api.Task

    import Tasks.ApiFixtures

    @invalid_attrs %{action: nil, str_value: nil, subject: nil, type: nil}

    test "list_tasks/0 returns all tasks" do
      task = task_fixture()
      assert Api.list_tasks() == [task]
    end

    test "get_task!/1 returns the task with given id" do
      task = task_fixture()
      assert Api.get_task!(task.id) == task
    end

    test "create_task/1 with valid data creates a task" do
      valid_attrs = %{
        action: "some action",
        str_value: "some str_value",
        subject: "some subject",
        type: "some type"
      }

      assert {:ok, %Task{} = task} = Api.create_task(valid_attrs)
      assert task.action == "some action"
      assert task.str_value == "some str_value"
      assert task.subject == "some subject"
      assert task.type == "some type"
    end

    test "create_task/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Api.create_task(@invalid_attrs)
    end

    test "update_task/2 with valid data updates the task" do
      task = task_fixture()

      update_attrs = %{
        action: "some updated action",
        str_value: "some updated str_value",
        subject: "some updated subject",
        type: "some updated type"
      }

      assert {:ok, %Task{} = task} = Api.update_task(task, update_attrs)
      assert task.action == "some updated action"
      assert task.str_value == "some updated str_value"
      assert task.subject == "some updated subject"
      assert task.type == "some updated type"
    end

    test "update_task/2 with invalid data returns error changeset" do
      task = task_fixture()
      assert {:error, %Ecto.Changeset{}} = Api.update_task(task, @invalid_attrs)
      assert task == Api.get_task!(task.id)
    end

    test "delete_task/1 deletes the task" do
      task = task_fixture()
      assert {:ok, %Task{}} = Api.delete_task(task)
      assert_raise Ecto.NoResultsError, fn -> Api.get_task!(task.id) end
    end

    test "change_task/1 returns a task changeset" do
      task = task_fixture()
      assert %Ecto.Changeset{} = Api.change_task(task)
    end
  end
end
